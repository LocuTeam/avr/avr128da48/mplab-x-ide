#include <xc.h>

// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin se v tom to registru nachází na í-té pozici, tedy na pozici 6

// Zadefinujeme makro PORTX, které bude reprezentovat, se kterým portem aktuálně pracujeme
#define PORTX PORTC

// Zadefinujeme makro LED, které bude reprezentovat náš pin PC6 -> 6. bit na portu C
#define LED 6

// Zadefinujeme makro BUTTON, které bude reprezentovat náš pin PC7 -> 7. bit na portu C
#define BUTTON 7

/* ************ Magie *********** */
#define PINXCTRL_STR(X) PIN##X##CTRL
#define PINXCTRL(N) PINXCTRL_STR(N)
/* ****************************** */


int main(void) {
    
    PORTX.DIR = PORTX.DIR | (1 << LED); // set LED as OUTPUT
    PORTX.OUT = PORTX.OUT | (1 << LED); // turn of LED

    PORTX.DIR = PORTX.DIR & ~(1 << BUTTON); // set BUTTON as INPUT
    PORTX.PINXCTRL(BUTTON) = 0b00001000;    // set pull-up
    
    while(1){
        
        if(((PORTX.IN >> BUTTON) & 1) == 0){
            PORTX.OUT = PORTX.OUT & ~(1 << LED);
        }
        else{
            PORTX.OUT = PORTX.OUT | (1 << LED);
        }
        
    }
    
    return 0;
}
