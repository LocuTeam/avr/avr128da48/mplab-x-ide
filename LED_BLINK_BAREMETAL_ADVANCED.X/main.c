// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7


// Inkludování základní knihovny (práce s registry, ...) pro XC8 překladač
#include <xc.h>

// Nadefinujeme makro F_CPU, které je očekáváno knihovnou delay.h -> výchozí rychlost CPU je 4MHz
#define F_CPU 4000000UL

// Inkluování základní knihovny pro práci se zpoždením
#include <util/delay.h>


// Nadefinujeme makro pro PORT, na kterém je připojená LED dioda
#define PORTX PORTC

// Nadefinujeme makro pro LED diodu
#define LED 6


int main(void) {
    
    PORTX.DIR = 1 << LED; // Nastavíme LED diodu jako výstup
    PORTX.OUT = 1 << LED; // Vypneme LED diodu
    
    while(1){
        
        PORTX.OUT = PORTX.OUT & ~(1 << LED); // Vypneme LED didodu
        
        _delay_ms(500);
        
        PORTX.OUT = PORTX.OUT | (1 << LED); // Zapneme LED diodu
        
        _delay_ms(500);
        
    }
    
    return 0;
}
