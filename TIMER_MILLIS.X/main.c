#include <xc.h>

#include "spse_iot/spse_iot.h"

/*
#include <stdio.h>

#define F_CPU 4000000
#include <util/delay.h>

#include "spse_iot/peripheral/plib_port/port.h"
#include "spse_iot/peripheral/sercom/usart/plib_sercom3_usart.h"
#include "spse_iot/peripheral/timer/tcb2.h"
*/


int main() {
    
    SPSE_IOT_Initialize(); // TCB2_Initialize(); // USER_LED_OutputEnable(); // USER_LED_Set(); // sei();
    
    SERCOM3_USART_Initialize(9600);

    
    while(1){

        static uint64_t nasledujici_cas = 0;
        uint64_t millis = TCB2_GetMillis();
        
        if(millis >= nasledujici_cas){
            nasledujici_cas = millis + 1000;
            USER_LED_Toggle();
            printf("t: %llus\r\n", millis / 1000);
        }

    }
    
    return 0;
}
