/*******************************************************************************
  MY DELAY Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    my_delay.c

  Summary:
    My delay library Source File
 
  Version:
    1.0 stable

  Description:
    This file provides basic functions for delay.

*******************************************************************************/


#include "mydelay.h"

void delay(uint32_t ms){
    while(ms--) _delay_ms(1);
}

void delay_us(uint32_t us){
    while(us--) _delay_us(1);
}
