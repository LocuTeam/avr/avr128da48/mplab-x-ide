#include <xc.h>

#define F_CPU 4000000
#include <util/delay.h>

// definice cisel v binarni soustave
uint16_t numbers[] = {  0b0111111101000000, 0b0111111101111001, 0b0111111000100100, 0b0111111000110000, 0b0111111000011001, // 0 1 2 3 4
                        0b0111111000010010, 0b0111111000000010, 0b0111111101111000, 0b0111111000000000, 0b0111111000010000, // 5 6 7 8 9
                        0b0111111000001000, 0b0111111000000011, 0b0111111101000110, 0b0111111000100001, 0b0111111000000110, // A B C D E
                        0b0111111000001110, 0b0111111101111111  };                                                          // F NAN

// definice znaku v binarni soustave
uint16_t chars[] = {    0b1111111010001000, 0b1101101011110000, 0b1111111111000110, 0b1101101111110000, 0b1111111110000110, // A B C D E
                        0b1111111110001110, 0b1111111011000010, 0b1111111010001001, 0b1101101111110110, 0b1111111111100001, // F G H I J
                        0b1110011110001111, 0b1111111111000111, 0b1111010111001001, 0b1110110111001001, 0b1111111111000000, // K L M N O
                        0b1111111010001100, 0b1110111111000000, 0b1110111010001100, 0b1111111010010010, 0b1101101111111110, // P Q R S T
                        0b1111111111000001, 0b1011011111001111, 0b1010111111001001, 0b1010010111111111, 0b1111111010010001, // U V W X Y
                        0b1011011111110110  };                                                                              // Z

// definice specialnich znaku v binarni soustave
uint16_t special_chars[] = {    0b1111101111111101, 0b1101101010110001, 0b1111101111111111, 0b1110011111111111, 0b1011110111111111, // " b ' < >
                                0b1000000010111111, 0b1101101010111111, 0b1011111111111111, 0b1111111010111111, 0b1011011111111111, // * + , - /
                                0b1101101111111111, 0b1110110111111111, 0b1101111001111100  };                                      // | \ ?

int main() {
    
    PORTC.DIR = 0b11111111; // nastav vsechny piny na portu C jako vystupni
    PORTD.DIR = 0b11111111; // nastav vsechny piny na portu D jako vystupni
    
    uint16_t num = 0x0000;
    
    while(1){
        
        uint8_t num4 = num / 4096;
        uint8_t num3 = (num / 256) - (num4 * 16);
        uint8_t num2 = (num / 16) - (num4 * 256) - (num3 * 16);
        uint8_t num1 = num - (num4 * 4096) - (num3 * 256) - (num2 * 16);
        
        // for cyklus bude trvat 16ms
        for(uint16_t i = 0; i < 100; ++i){
            
            // aktivovat prvni cast digit0
            PORTD.OUT = 0b00000001;
            PORTC.OUT = numbers[num4] & 0xFF;

            _delay_us(100);
            
            // deaktivovat vsech digitu
            PORTD.OUT = 0b00000000;
            PORTC.OUT = 0xFF;

            _delay_us(100);
            
            // aktivovat druha cast digit0
            PORTD.OUT = 0b00000010;
            PORTC.OUT = (numbers[num4] >> 8) & 0xFF;

            _delay_us(100);
            
            // deaktivovat vsech digitu
            PORTD.OUT = 0b00000000;
            PORTC.OUT = 0xFF;

            _delay_us(100);

            // aktivovat prvni cast digit1
            PORTD.OUT = 0b00000100;
            PORTC.OUT = numbers[num3] & 0xFF;

            _delay_us(100);

            // deaktivovat vsech digitu
            PORTD.OUT = 0b00000000;
            PORTC.OUT = 0xFF;

            _delay_us(100);

            // aktivovat druha cast digit1
            PORTD.OUT = 0b00001000;
            PORTC.OUT = (numbers[num3] >> 8) & 0xFF;

            _delay_us(100);

            // deaktivovat vsech digitu
            PORTD.OUT = 0b00000000;
            PORTC.OUT = 0xFF;

            _delay_us(100);

            // aktivovat prvni cast digit2
            PORTD.OUT = 0b00010000;
            PORTC.OUT = numbers[num2] & 0xFF;

            _delay_us(100);

            // deaktivovat vsech digitu
            PORTD.OUT = 0b00000000;
            PORTC.OUT = 0xFF;

            _delay_us(100);

            // aktivovat druhou cast digit2
            PORTD.OUT = 0b00100000;
            PORTC.OUT = (numbers[num2] >> 8) & 0xFF;

            _delay_us(100);

            // deaktivovat vsech digitu
            PORTD.OUT = 0b00000000;
            PORTC.OUT = 0xFF;

            _delay_us(100);

            // aktivovat prvni cast digit3
            PORTD.OUT = 0b01000000;
            PORTC.OUT = numbers[num1] & 0xFF;

            _delay_us(100);

            // deaktivovat vsech digitu
            PORTD.OUT = 0b00000000;
            PORTC.OUT = 0xFF;

            _delay_us(100);

            // aktivovat druhou cast digit3
            PORTD.OUT = 0b10000000;
            PORTC.OUT = (numbers[num1] >> 8) & 0xFF;

            _delay_us(100);

            // deaktivovat vsech digitu
            PORTD.OUT = 0b00000000;
            PORTC.OUT = 0xFF;

            _delay_us(100);
        }
        
        if(num >= 0xFFFF) num = 0;
        else ++num;
        
    }
    
    return 0;
}
