// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7


// Inkludování základní knihovny (práce s registry, ...) pro XC8 překladač
#include <xc.h>

// Nadefinujeme makro F_CPU, které je očekáváno knihovnou delay.h -> výchozí rychlost CPU je 4MHz
#define F_CPU 4000000UL

// Inkluování základní knihovny pro práci se zpoždením
#include <util/delay.h>


int main(void) {
    
    PORTB.DIR = 0b01000000; // Nastavíme LED diodu jako výstup
    PORTB.OUT = 0b01000000; // Vypneme LED diodu
    
    while(1){
        
        PORTB.OUT = 0b00000000; // Zapneme LED diodu
        
        _delay_ms(500);
        
        PORTB.OUT = 0b01000000; // Vypneme LED diodu
        
        _delay_ms(500);
        
    }
    
    return 0;
}
