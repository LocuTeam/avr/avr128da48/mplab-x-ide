#include <xc.h>
#include <stdio.h>

#include "spse_iot/spse_iot.h"


// makra pro pou?�v�n� Arduino funkci a maker

#define OUTPUT 1
#define INPUT 0
#define INPUT_PULLUP 2

#define pinMode(PIN, MODE) (MODE == OUTPUT) ? (PORT_PinOutputEnable(PIN)) : (MODE == INPUT) ? (PORT_PinInputEnable(PIN)) : (PORT_PinInputPullupEnable(PIN))
#define digitalWrite(PIN, LEVEL) (PORT_PinWrite(PIN, (_Bool) LEVEL))
#define digitalRead(PIN) (PORT_PinRead(PIN))

#define Serial3_begin(BAUDRATE) (SERCOM3_USART_Initialize(BAUDRATE))



// funkce pro aktivaci sloupe?ku
void set_column(uint8_t column){
    switch(column){
        case 0:
            pinMode(PORT_PIN_PD00, OUTPUT);
            digitalWrite(PORT_PIN_PD00, LOW);
        break;
        
        case 1:
            pinMode(PORT_PIN_PD01, OUTPUT);
            digitalWrite(PORT_PIN_PD01, LOW);
        break;
        
        case 2:
            pinMode(PORT_PIN_PD02, OUTPUT);
            digitalWrite(PORT_PIN_PD02, LOW);
        break;
        
        case 3:
            pinMode(PORT_PIN_PD03, OUTPUT);
            digitalWrite(PORT_PIN_PD03, LOW);
        break;
    }
}

// funkce pro vypnut� sloupe?ku
void clear_column(uint8_t column){
    switch(column){
        case 0:
            pinMode(PORT_PIN_PD00, INPUT_PULLUP);
        break;
        
        case 1:
            pinMode(PORT_PIN_PD01, INPUT_PULLUP);
        break;
        
        case 2:
            pinMode(PORT_PIN_PD02, INPUT_PULLUP);
        break;
        
        case 3:
            pinMode(PORT_PIN_PD03, INPUT_PULLUP);
        break;
    }
}

// funkce na zjisten� stavu r�dku
bool is_active_row(uint8_t row){
    bool active = true;
    switch(row){
        case 0:
            active = digitalRead(PORT_PIN_PD04);
        break;
        
        case 1:
            active = digitalRead(PORT_PIN_PD05);
            break;
        
        case 2:
            active = digitalRead(PORT_PIN_PD06);
        break;
        
        case 3:
            active = digitalRead(PORT_PIN_PD07);
        break;
    }
    return !active;
}


// funkce, kter� zjist�, jestli doslo na maticov� kl�vesnici ke zmene
uint8_t is_change(uint8_t p0[], uint8_t p1[]){
    for(size_t i = 0; i < 16; ++i)
        if(p0[i] != p1[i]) return true;
    return false;
}

// funkce zkop�ruje veechny prvky jednoho pole do druh�ho pole
void copy_change(uint8_t p0[], uint8_t p1[]){
    for(size_t i = 0; i < 16; ++i)
        p0[i] = p1[i];
}

// funkce kontroluje, ze je stisknuto pouze jedno tlac�tko
bool is_valid(uint8_t p[]){
    uint8_t _cnt = 1;
    for(size_t i = 0; i < 16; ++i)
        if(p[i] == 1){
            if(_cnt == 0) return false;
            else --_cnt;
        }
    return true;
}

// funkce pro zjisten�, jestli je nejak� tlac�tko aktivn� a pokud ano, tak vrac� index zm�cknut�ho tlac�tka, pokud ne, tak vrac� c�slo 16
uint8_t get_activated_index(uint8_t p[]){
    for(uint8_t i = 0; i < 4; ++i)
        for(uint8_t k = 0; k < 4; ++k)
            if(p[i + k * 4]) return (i + k * 4);
    return 16;
}

int main() {
    
    // zah�jen� komunikace s periferi� USART
    Serial3_begin(9600);
    
    // nastaven� sloupecku
    pinMode(PORT_PIN_PD00, INPUT_PULLUP); // nult� sloupecek
    pinMode(PORT_PIN_PD01, INPUT_PULLUP); // prvn� sloupecek
    pinMode(PORT_PIN_PD02, INPUT_PULLUP); // druh� sloupecek
    pinMode(PORT_PIN_PD03, INPUT_PULLUP); // tret� sloupecek
    
    // nastaven� r�dku
    pinMode(PORT_PIN_PD04, INPUT_PULLUP); // nult� r�dek
    pinMode(PORT_PIN_PD05, INPUT_PULLUP); // prvn� r�dek
    pinMode(PORT_PIN_PD06, INPUT_PULLUP); // druh� r�dek
    pinMode(PORT_PIN_PD07, INPUT_PULLUP); // tret� r�dek
    
    // nastaven� uzivatelsk� LED diody
    pinMode(USER_LED_PIN, OUTPUT);
    digitalWrite(USER_LED_PIN, HIGH);
    
    
    // pomocn� promenn� pro zjistov�n� zmen
    uint8_t last_activated_matrix[16] = {};
    
    // definice znaku pro jednotliv� tlac�tka na marticov� kl�vesnici
    const char activated_matrix_chars[16] = {   '*', '0', '#', 'D',     // nult� r�dek
                                                '7', '8', '9', 'C',     // prvn� r�dek
                                                '4', '5', '6', 'B',     // druh� r�dek
                                                '1', '2', '3', 'A' };   // tret� r�dek
    
    
    const char pin[4] = {'1', '2', '3', 'A'}; // uzivatelsk� pin
    const char enter = '#'; // potvrzovac� tlac�tko
    const uint8_t max_attemps = 3; // pocet povolen�ch chybn�ch zad�n� pinu
    
    while(1){
        
        
        // cteme vsechna tlac�tka a ukl�d�me do pole stavy
        
        uint8_t activated_matrix[16] = {};
        
        for(uint8_t i = 0; i < 4; ++i){
            set_column(i); // aktivujeme sloupecek
            for(uint8_t p = 0; p < 4; ++p){
                if(is_active_row(p)) activated_matrix[i + (4 * p)] = 1; // cteme postupne vsechny 4 r�dky na aktivovan�m sloupecku
            }
            clear_column(i); // vypneme sloupecek
        }
        
        
        // vyhodnocujeme precten� stavy jednotliv�ch tlac�tek
        
        static uint8_t pressed_flag = 0;
        static char pressed_char    = 0;
        
        if(is_change(activated_matrix, last_activated_matrix)){             // reagujeme pouze na zmenu
            if(is_valid(activated_matrix)){                                 // je aktivovan� pouze jedno tlac�tko?
                copy_change(last_activated_matrix, activated_matrix);       // zkop�rujeme aktivovan� tlac�tka do pomocn� promenn�, kter� n�m slouz� pro porovn�v�n� zmen
                uint8_t index;
                if((index = get_activated_index(activated_matrix)) != 16){  // pokud je alespon jedno tlac�tko aktivn�, tak z�sk�me index tlac�tka pro pole
                    pressed_flag = 1;                                       // nastav�me promennou pressed_flag na 1 -> doslo k �spesn�mu precten� hodnoty
                    pressed_char = activated_matrix_chars[index];           // nastav�me znak aktu�lne aktivn�ho tlac�tka
                }
            }
        }
        
        
        
        // kontrola uzivatelsk�ho pin k�du
        
        static uint8_t lock_state    = 0; // prom?nn� pro uchov�v�n� aktu�ln�ho stavu zpracov�v�n� vstupu
        static uint8_t wrong_try_cnt = 0; // prom?nn� pro poc�t�n� chybn�ch vstup?
        
        switch(lock_state){
            
            case 0: // zac�n�me a cek�me na stisknut� prvn�ho tlac�tka
                if(pressed_flag){
                    pressed_flag = 0;
                    if(pressed_char == pin[0]) lock_state = 1; // pokud se shoduje stisknut� znak tlac�tka s nas�m pinem tak jdeme do stavu 1
                    else lock_state = 5; // pokud jsme zadali chybn� prvn� znak pinu, jdeme do stavu 5
                }
            break;
            
            case 1: // kontrolujeme druh� zm�?knut� tla?�tko
                if(pressed_flag){
                    pressed_flag = 0;
                    if(pressed_char == pin[1]) lock_state = 2; // pokud se shoduje druh� stisknut� znak tlac�tka s nas�m pinem tak jdeme do stavu 2
                    else lock_state = 5; // pokud jsme zadali chybn� druh� znak pinu, jdeme do stavu 5
                }
            break;
            
            case 2: // kontrolujeme tret� zm�cknut� tlac�tko
                if(pressed_flag){
                    pressed_flag = 0;
                    if(pressed_char == pin[2]) lock_state = 3; // pokud se shoduje tret� stisknut� znak tlac�tka s nas�m pinem tak jdeme do stavu 3
                    else lock_state = 5; // pokud jsme zadali chybn� tret� znak pinu, jdeme do stavu 5
                }
            break;
            
            case 3: // kontrolujeme ctvrt� zm�cknut� tlac�tko
                if(pressed_flag){
                    pressed_flag = 0;
                    if(pressed_char == pin[3]) lock_state = 4; // pokud se shoduje ctvrt� stisknut� znak tlac�tka s nas�m pinem tak jdeme do stavu 4
                    else lock_state = 5; // pokud jsme zadali chybn� ctvrt� znak pinu, jdeme do stavu 5
                }
            break;
            
            case 4: // kontrolujeme potvrzen� pinu
                if(pressed_flag){
                    pressed_flag = 0;
                    if(pressed_char == enter) lock_state = 6; // pokud se shoduje potvrzovac� stisknut� znak tlac�tka, tak jdeme do stavu 6
                    else lock_state = 5; // pokud jsme zadali chybn� potvrzovac� znak, tak jdeme do stavu 5
                }
            break;
            
            case 5: // v predesle zad�van�ch znac�ch doslo k chybe a zde cek�me na potvrzovac� znak
                if(pressed_flag){
                    pressed_flag = 0;
                    if(pressed_char == enter) lock_state = 7; // pokud se shoduje potvrzovac� stisknut� znak tlac�tka, tak jdeme do stavu 6
                }
            break;
            
            case 6: // odemkneme zar�zen�
                
                printf("unlock :-)\r\n");
                digitalWrite(USER_LED_PIN, LOW);
                wrong_try_cnt = 0;
                lock_state = 0;
                
            break;
            
            case 7: // dostaneme se sem, kdyz doslo k chybe zadan�mu vstupu a my jsme ho potvrdili
                if(wrong_try_cnt >= max_attemps){ // pokud pres�hneme maxim�ln� mozn� pocet pokusu jdeme do stavu 8
                    printf("locked forever :-(\r\n");
                    digitalWrite(USER_LED_PIN, HIGH);
                    lock_state = 8;
                }
                else{
                    printf("wrong input!\r\n");
                    digitalWrite(USER_LED_PIN, HIGH);
                    ++wrong_try_cnt;
                    lock_state = 0;
                }
            break;
            
            case 8: // zde zustaneme az do konce programu, protoze jsme zadali chybn� pin v�ce kr�t, nez je maxim�ln� mozn� pocet zad�v�n� pinu
                // nothing
            break;
            
            default:
                printf("error\r\n");
                lock_state = 0;
            break;
            
        }
        
    }
    
    return 0;
}
