/*******************************************************************************
  SPSE IOT Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    spse_iot.c

  Summary:
    SPSE IoT library Source File
 
  Version:
    1.0 stable

  Description:
    This file provides basic functions.

*******************************************************************************/


#include "spse_iot.h"

