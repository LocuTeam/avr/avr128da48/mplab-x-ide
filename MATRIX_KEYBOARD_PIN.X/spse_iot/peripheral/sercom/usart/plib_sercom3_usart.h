/*******************************************************************************
  PLIB SERCOM3 USART Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    plib_sercom3_usart.h

  Summary:
    PLIB SERCOM3 USART Header File
 
  Version:
    1.0 stable

  Description:
    This file provides basic functions for USART communication.

*******************************************************************************/


#ifndef PLIB_SERCOM3_USART_H
#define PLIB_SERCOM3_USART_H


#include <stdint.h>
#include <stdbool.h>


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
    extern "C" {
#endif
// DOM-IGNORE-END

        
#define USART_BAUD_RATE(BAUD_RATE)  ((float)(64 * 4000000 / (16 * (float)BAUD_RATE)) + 0.5)


void SERCOM3_USART_Initialize(uint16_t baudrate);
bool SERCOM3_USART_TransmitterIsReady();
void SERCOM3_USART_WriteByte(int data);
void SERCOM3_USART_Write(void *buffer, const size_t size);
bool SERCOM3_USART_ReceiverIsReady();
int SERCOM3_USART_ReadByte();
void SERCOM3_USART_Read(void *buffer, const size_t size);


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

}

#endif
// DOM-IGNORE-END

#endif // PLIB_SERCOM3_USART_H
