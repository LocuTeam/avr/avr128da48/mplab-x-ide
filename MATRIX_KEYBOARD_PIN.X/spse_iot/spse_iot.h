/*******************************************************************************
  SPSE IOT Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    spse_iot.h

  Summary:
    SPSE IoT library Header File
 
  Version:
    1.0 stable

  Description:
    This file provides basic functions.

*******************************************************************************/


#ifndef SPSE_IOT_H
#define SPSE_IOT_H


#include "mydelay/mydelay.h"
#include "peripheral/plib_port/port.h"
#include "lcd_display_driver/lcd_display_driver.h"
#include "peripheral/sercom/usart/plib_sercom3_usart.h"


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
    extern "C" {
#endif
// DOM-IGNORE-END


inline long limit_value(long in, long min, long max){
    return (in < min) ? min : (in > max) ? max : in;
}

inline unsigned long limit_value_unsigned(unsigned long in, unsigned long min, unsigned long max){
    return (in < min) ? min : (in > max) ? max : in;
}

inline long map_value(long in, long min1, long min2, long max1, long max2){
    return (in - min1) * (max2 - min2) / (max1 - min1) + min2;
}

inline unsigned long map_value_unsigned(unsigned long in, unsigned long min1, unsigned long min2, unsigned long max1, unsigned long max2){
    return (in - min1) * (max2 - min2) / (max1 - min1) + min2;
}



// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

}

#endif
// DOM-IGNORE-END

#endif // SPSE_IOT_H
