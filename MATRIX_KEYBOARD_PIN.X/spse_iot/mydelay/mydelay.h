/*******************************************************************************
  MY DELAY Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    my_delay.h

  Summary:
    My delay library Header File
 
  Version:
    1.0 stable

  Description:
    This file provides basic functions for delay.

*******************************************************************************/


#ifndef MY_DELAY_H
#define MY_DELAY_H


#ifndef F_CPU
    #define F_CPU 4000000
#endif
#include <util/delay.h>


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
    extern "C" {
#endif
// DOM-IGNORE-END

        
void delay(uint32_t ms);
void delay_us(uint32_t us);


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

}

#endif
// DOM-IGNORE-END

#endif // MY_DELAY_H
