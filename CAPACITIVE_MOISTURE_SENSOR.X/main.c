#include <xc.h>

#include <stdio.h>

#include "spse_iot/spse_iot.h"

#include "spse_iot/easy_delay/easy_delay.h"
#include "spse_iot/peripheral/sercom/usart/plib_sercom3_usart.h"
#include "spse_iot/peripheral/adc/adc0.h"

int main() {
    
    USER_LED_OutputEnable();
    USER_LED_Set();
    
    ADC0_Initialize();
    
    while(1){
        
        ADC0_StartConversion(ADC_CHANEL_AIN0);
        while(!ADC0_IsConversionDone());
        uint16_t val = ADC0_ReadData();
        
        if(val > 2000){
            USER_LED_Set(); // Dry
        }
        else{
            USER_LED_Clear(); // Wet
        }
        
        delay(250);
        
    }
    
    return 0;
}
