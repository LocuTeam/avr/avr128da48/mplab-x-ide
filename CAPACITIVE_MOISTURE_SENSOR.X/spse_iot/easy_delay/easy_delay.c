/*******************************************************************************
  EASY DELAY Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    easy_delay.c

  Summary:
    Easy delay library Source File
 
  Version:
    1.1 stable

  Description:
    This file provides basic functions for delay.

*******************************************************************************/


#include "easy_delay.h"

void delay(uint32_t ms){
    while(ms--) _delay_ms(1);
}

void delay_us(uint32_t us){
    while(us--) _delay_us(1);
}
