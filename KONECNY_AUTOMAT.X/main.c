#include <xc.h>
#include <avr/interrupt.h>
#include "spse_iot/spse_iot.h"


bool cti_tlacitko(){
    static uint8_t stav = 0; //  počáteční stav

    switch(stav){
        case 0:
            if(((PORTB.IN >> 2) & 1) == 0) stav = 1; // přejdeme do dalšího stavu
            return false;
        break;

        case 1:
            if(((PORTB.IN >> 2) & 1) == 1) stav = 2; // přejdeme do dalšího stavu
            return false;
        break;

        case 2:
            stav = 0; // přejdeme do prvního stavu
            return true;
        break;
    }
    return false;
}

void blikej_led(){
    static uint8_t stav = 0; //  počáteční stav
    static uint64_t ncas = 0;

    switch(stav){
        case 0:
            PORTB.OUT |= 0b00001000;
            ncas = TCB2_GetMillis() + 500;
            stav = 1; // přejdeme do dalšího stavu
        break;

        case 1:
            if(TCB2_GetMillis() >= ncas) stav = 2; // přejdeme do dalšího stavu
        break;

        case 2:
            PORTB.OUT &= 0b11110111;
            ncas = TCB2_GetMillis() + 500;
            stav = 3; // přejdeme do dalšího stavu
        break;

        case 3:
            if(TCB2_GetMillis() >= ncas) stav = 0; // přejdeme do prvního stavu
        break;
    }
}




int main(){

    USER_LED_OutputEnable();
    USER_LED_Set();

    USER_BUTTON_InputEnable();
    PORTB.PIN2CTRL = 0b00001000;

    TCB2_Initialize();
    
    sei();

    while(1){

        if(cti_tlacitko() == true){
            USER_LED_Set();
        }

        blikej_led(); // definování proměnné a s hodnotou 10 
    }
    return 0;
}

