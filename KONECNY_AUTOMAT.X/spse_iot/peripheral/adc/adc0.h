/*******************************************************************************
  ADC0 Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    adc0.h

  Summary:
    ADC0 library Header File
 
  Version:
    1.0 stable

  Description:
    This file provides basic functions.

*******************************************************************************/

#ifndef ADC0_H
#define	ADC0_H

#include <stdint.h>
#include <stdbool.h>

#ifdef	__cplusplus
extern "C" {
#endif


typedef enum{
    ADC_CHANEL_AIN0        = 0x00,
    ADC_CHANEL_AIN1        = 0x01,
    ADC_CHANEL_AIN2        = 0x02,
    ADC_CHANEL_AIN3        = 0x03,
    ADC_CHANEL_AIN4        = 0x04,
    ADC_CHANEL_AIN5        = 0x05,
    ADC_CHANEL_AIN6        = 0x06,
    ADC_CHANEL_AIN7        = 0x07,
    ADC_CHANEL_AIN8        = 0x08,
    ADC_CHANEL_AIN9        = 0x09,
    ADC_CHANEL_AIN10       = 0x0A,
    ADC_CHANEL_AIN11       = 0x0B,
    ADC_CHANEL_AIN12       = 0x0C,
    ADC_CHANEL_AIN13       = 0x0D,
    ADC_CHANEL_AIN14       = 0x0E,
    ADC_CHANEL_AIN15       = 0x0F,
    ADC_CHANEL_AIN16       = 0x10,
    ADC_CHANEL_AIN17       = 0x11,
    ADC_CHANEL_AIN18       = 0x12,
    ADC_CHANEL_AIN19       = 0x13,
    ADC_CHANEL_AIN20       = 0x14,
    ADC_CHANEL_AIN21       = 0x15,
    ADC_CHANEL_GND         = 0x40,
    ADC_CHANEL_TEMPERATURE = 0x42,
    ADC_CHANEL_VDDDIV10    = 0x44,
    ADC_CHANEL_VDDIO2DIV10 = 0x45,
    ADC_CHANEL_DAC0        = 0x48,
    ADC_CHANEL_DACREF0     = 0x49,
    ADC_CHANEL_DACREF1     = 0x4A,
    ADC_CHANEL_DACREF2     = 0x4B
} ADC_CHANEL_e;


void ADC0_Initialize();
bool ADC0_StartConversion(ADC_CHANEL_e chanel);
void ADC0_StopConversion();
bool ADC0_IsConversionDone();
uint16_t ADC0_ReadData();
int8_t ADC0_ReadTemperature();

#ifdef	__cplusplus
}
#endif

#endif	/* ADC0_H */

