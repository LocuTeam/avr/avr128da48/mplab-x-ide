/*******************************************************************************
  ADC0 Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    adc0.c

  Summary:
    ADC0 library Source File
 
  Version:
    1.0 stable

  Description:
    This file provides basic functions.

*******************************************************************************/

#include "adc0.h"
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

void ADC0_Initialize(){
    VREF.ADC0REF = VREF_ALWAYSON_bm | VREF_REFSEL_VDD_gc;
    
    ADC0.CTRLA |= ADC_ENABLE_bm;
    ADC0.CTRLB = 0;
    ADC0.CTRLC = ADC_PRESC_DIV2_gc;
    ADC0.CTRLD = ADC_SAMPDLY_DLY1_gc | ADC_INITDLY_DLY16_gc;
    ADC0.CTRLE = ADC_WINCM_NONE_gc;
    
    ADC0.SAMPCTRL = ADC_SAMPLEN_2_bm;
    
    ADC0.EVCTRL  = 0;
    ADC0.INTCTRL = ADC_RESRDY_bm;
}

bool ADC0_StartConversion(ADC_CHANEL_e chanel){
    if(     ((chanel >= 0x16) && (chanel <= 0x3F))
        ||  (chanel == 0x41)
        ||  (chanel == 0x43)
        ||  (chanel == 0x46)
        ||  (chanel == 0x47)
        ||  (chanel >= 0x4C)) return false;
    
    ADC0.MUXPOS  = chanel;        // select chanel pin
    ADC0.COMMAND = ADC_STCONV_bm; // start conversion
    
    return true;
}

void ADC0_StopConversion(){
    ADC0.COMMAND = ADC_SPCONV_bm;
}

bool ADC0_IsConversionDone(){
    return (ADC0.INTFLAGS & 0x01);
}

uint16_t ADC0_ReadData(){
    return ((ADC0.RESH << 8) | ADC0.RESL);
}

int8_t ADC0_ReadTemperature(){
    const uint8_t _vref = VREF.ADC0REF;
    VREF.ADC0REF = VREF_ALWAYSON_bm | VREF_REFSEL_2V048_gc;
    uint16_t sigrow_offset = SIGROW.TEMPSENSE1;
    uint16_t sigrow_slope  = SIGROW.TEMPSENSE0;
    ADC0_StartConversion(ADC_CHANEL_TEMPERATURE);
    while(!ADC0_IsConversionDone());
    uint16_t adc_reading = ADC0_ReadData();
    uint32_t temp = sigrow_offset - adc_reading;
    temp *= sigrow_slope;
    temp += 0x0800;
    temp >>= 12;
    VREF.ADC0REF = _vref;
    return (((int16_t) temp) - 273);
}