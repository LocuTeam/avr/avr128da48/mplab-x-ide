/*******************************************************************************
  LCD DISPLAY DRIVER

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    lcd_display_driver.c

  Summary:
    LCD display Source File
 
  Version:
    1.1 stable

  Description:
    This file provides basic functions for LCD display control.

*******************************************************************************/


#include "lcd_display_driver.h"
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>


static void _lcd_display_write_byte(const lcd_display_t const *me, uint8_t byte, uint8_t rs) {
    if(rs == 1) PORT_PinWrite(me->pins.comm.RS, HIGH);
    else PORT_PinWrite(me->pins.comm.RS, LOW);
    
    PORT_PinWrite(me->pins.comm.EN, HIGH);
    
    PORT_PinWrite(me->pins.data.D0, (byte >> 0) & 1);
    PORT_PinWrite(me->pins.data.D1, (byte >> 1) & 1);
    PORT_PinWrite(me->pins.data.D2, (byte >> 2) & 1);
    PORT_PinWrite(me->pins.data.D3, (byte >> 3) & 1);
    PORT_PinWrite(me->pins.data.D4, (byte >> 4) & 1);
    PORT_PinWrite(me->pins.data.D5, (byte >> 5) & 1);
    PORT_PinWrite(me->pins.data.D6, (byte >> 6) & 1);
    PORT_PinWrite(me->pins.data.D7, (byte >> 7) & 1);

    PORT_PinWrite(me->pins.comm.EN, LOW);
    delay_us(1);
    PORT_PinWrite(me->pins.comm.EN, HIGH);
    delay_us(500);
}


static void _lcd_display_write_halfbyte(const lcd_display_t const *me, uint8_t halfbyte, uint8_t rs) {
    if(rs == 1) PORT_PinWrite(me->pins.comm.RS, HIGH);
    else PORT_PinWrite(me->pins.comm.RS, LOW);
    
    PORT_PinWrite(me->pins.comm.EN, HIGH);
    
    PORT_PinWrite(me->pins.data.D0, (halfbyte >> 0) & 1);
    PORT_PinWrite(me->pins.data.D1, (halfbyte >> 1) & 1);
    PORT_PinWrite(me->pins.data.D2, (halfbyte >> 2) & 1);
    PORT_PinWrite(me->pins.data.D3, (halfbyte >> 3) & 1);

    PORT_PinWrite(me->pins.comm.EN, LOW);
    delay_us(1);
    PORT_PinWrite(me->pins.comm.EN, HIGH);
    delay_us(500);
}


static inline void _lcd_display_write_cursor(const lcd_display_t const *me){
    _lcd_display_write_byte(me, 0x80 + ((me->pos.y) ? 0x40 : 0x00) + (me->pos.x), 0);
}


static inline void _lcd_display_increment_cursor(lcd_display_t *me){
    if((me->pos.x + 1) >= me->config.dimensions.x){
        me->pos.x = 0;
        if((me->pos.y + 1) >= me->config.dimensions.y) me->pos.y = 0;
        else ++me->pos.y;
    }
    else ++me->pos.x;
}


bool lcd_display_set_cursor(lcd_display_t *me, uint8_t x, uint8_t y){
    if(((x + 1) >= me->config.dimensions.x) || ((y + 1) > me->config.dimensions.y)) return false;
    
    me->pos.x = x;
    me->pos.y = y;
    
    if(me->config.entry_mode.increment == LCD_DISPLAY_ENTRY_MODE_INCREMENT_INC) _lcd_display_write_cursor(me);
    
    return true;
}


bool lcd_display_get_cursor(const lcd_display_t const *me, uint8_t *x, uint8_t *y){
    *x = me->pos.x;
    *y = me->pos.y;
    
    return true;
}


bool lcd_display_write_char(lcd_display_t *me, const char c){
    if(!isprint(c)) return false;
    
    if(     me->config.entry_mode.increment == LCD_DISPLAY_ENTRY_MODE_INCREMENT_SOFTINC
        ||  me->config.entry_mode.increment == LCD_DISPLAY_ENTRY_MODE_INCREMENT_NOINC ) _lcd_display_write_cursor(me);
    
    _lcd_display_write_byte(me, (uint8_t) c, 1);
    
    if(     me->config.entry_mode.increment == LCD_DISPLAY_ENTRY_MODE_INCREMENT_SOFTINC
        ||  me->config.entry_mode.increment == LCD_DISPLAY_ENTRY_MODE_INCREMENT_NOINC ) _lcd_display_increment_cursor(me);
    
    return true;
}


bool lcd_display_write_char_xy(lcd_display_t *me, const char c, const uint8_t x, const uint8_t y){
    if(!isprint(c)) return false;
    
    if(!lcd_display_set_cursor(me, x, y)) return false;
    if(!lcd_display_write_char(me, c)) return false;
    
    return true;
}


bool lcd_display_write_string(lcd_display_t *me, const char *s){
    
    bool _state = true;
    
    while(*s){
        if(!lcd_display_write_char(me, *s)){
            _state = false;
        }
        ++s;
    }
    
    return _state;
}


bool lcd_display_clear_xy(lcd_display_t *me, const uint8_t x, const uint8_t y){
    if(!lcd_display_set_cursor(me, x, y)) return false;
    if(!lcd_display_write_char(me, ' ')) return false;
    return true;
}


bool lcd_display_clear_row(lcd_display_t *me, const uint8_t y){
    bool _state = true;
    
    if(!lcd_display_set_cursor(me, 0, y)) return false;
    
    for(uint8_t i = 0; i < me->config.dimensions.x; ++i){
        if(!lcd_display_write_char(me, ' ')){
            _state = false;
            break;
        }
    }
    return _state;
}


bool lcd_display_clear(lcd_display_t *me){
    bool _state = true;
    
    for(uint8_t i = 0; i < me->config.dimensions.y; ++i){
        if(!lcd_display_clear_row(me, i)){
            _state = false;
            break;
        }
    }
    return _state;
}


bool lcd_display_init(lcd_display_t *me, const lcd_display_config_t const *cfg, const lcd_display_pins_t const *pins, const uint8_t init_x, const uint8_t init_y) {
    
    me->config.function_set.data_mode = cfg->function_set.data_mode;
    me->config.function_set.line_mode = cfg->function_set.line_mode;
    me->config.function_set.font_size = cfg->function_set.font_size;
    
    me->config.display_control.power    = cfg->display_control.power;
    me->config.display_control.cursor   = cfg->display_control.cursor;
    me->config.display_control.blinking = cfg->display_control.blinking;
    
    me->config.entry_mode.increment = cfg->entry_mode.increment;
    me->config.entry_mode.shift     = cfg->entry_mode.shift;
    
    me->config.dimensions.x = cfg->dimensions.x;
    me->config.dimensions.y = cfg->dimensions.y;
    
    
    me->pos.x = init_x;
    me->pos.y = init_y;
    
    me->pins.comm.EN = pins->comm.EN;
    me->pins.comm.RS = pins->comm.RS;
    me->pins.data.D0 = pins->data.D0;
    me->pins.data.D1 = pins->data.D1;
    me->pins.data.D2 = pins->data.D2;
    me->pins.data.D3 = pins->data.D3;
    me->pins.data.D4 = pins->data.D4;
    me->pins.data.D5 = pins->data.D5;
    me->pins.data.D6 = pins->data.D6;
    me->pins.data.D7 = pins->data.D7;
    
    
    PORT_PinOutputEnable(me->pins.comm.RS);
    PORT_PinOutputEnable(me->pins.comm.EN);
    
    PORT_PinOutputEnable(me->pins.data.D0);
    PORT_PinOutputEnable(me->pins.data.D1);
    PORT_PinOutputEnable(me->pins.data.D2);
    PORT_PinOutputEnable(me->pins.data.D3);
    PORT_PinOutputEnable(me->pins.data.D4);
    PORT_PinOutputEnable(me->pins.data.D5);
    PORT_PinOutputEnable(me->pins.data.D6);
    PORT_PinOutputEnable(me->pins.data.D7);
    
    
    delay(1); // power up wait
    
    
    uint8_t function_set = 0b00100000;
    if(me->config.function_set.data_mode == LCD_DISPLAY_FUNCTION_SET_DATA_MODE_8b)      function_set |= 0b00010000; // setup 8bit communication
    if(me->config.function_set.line_mode == LCD_DISPLAY_FUNCTION_SET_LINE_MODE_2LINE)   function_set |= 0b00001000; // setup 2 lines view
    if(me->config.function_set.font_size == LCD_DISPLAY_FUNCTION_SET_FONT_SIZE_5X10)    function_set |= 0b00000100; // setup font size
    
    uint8_t display_control = 0b00001000;
    if(me->config.display_control.power == LCD_DISPLAY_CONTROL_POWER_ON)        display_control |= 0b00000100; // turn on display
    if(me->config.display_control.cursor == LCD_DISPLAY_CONTROL_CURSOR_ON)      display_control |= 0b00000010; // turn on cursor
    if(me->config.display_control.blinking == LCD_DISPLAY_CONTROL_BLINKING_ON)  display_control |= 0b00000001; // turn on blinking
    
    uint8_t entry_mode = 0b00000100;
    if(me->config.entry_mode.increment == LCD_DISPLAY_ENTRY_MODE_INCREMENT_INC) entry_mode |= 0b00000010; // turn on increment
    if(me->config.entry_mode.shift == LCD_DISPLAY_ENTRY_MODE_SHIFT_SH)          entry_mode |= 0b00000001; // turn on shift
    
    
    if(me->config.function_set.data_mode == LCD_DISPLAY_FUNCTION_SET_DATA_MODE_8b){
        _lcd_display_write_byte(me, function_set, 0); // set 8-bit mode, 2 lines, 5x8 font
        
        _lcd_display_write_byte(me, 0b00000001, 0); // clear whole display
        
        _lcd_display_write_byte(me, display_control, 0);
        
        _lcd_display_write_byte(me, entry_mode, 0);
    }
    if(me->config.function_set.data_mode == LCD_DISPLAY_FUNCTION_SET_DATA_MODE_4b){
        _lcd_display_write_byte(me, 0b00000010, 0); // switch to 4-bit mode
        
        _lcd_display_write_halfbyte(me, function_set >> 4, 0);
        _lcd_display_write_halfbyte(me, function_set & 0xFF, 0);
        
        _lcd_display_write_halfbyte(me, display_control >> 4, 0);
        _lcd_display_write_halfbyte(me, display_control & 0xFF, 0);
        
        _lcd_display_write_halfbyte(me, entry_mode >> 4, 0);
        _lcd_display_write_halfbyte(me, entry_mode & 0xFF, 0); 
    }
    
    return true;
}

