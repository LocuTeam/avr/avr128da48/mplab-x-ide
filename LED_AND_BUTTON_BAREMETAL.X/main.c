#include <xc.h>

// Vyvojov� deska AVR128DA48 m� p?ipojenou LED diodu na pinu PC6 a tla?�tko na pinu PC7.
// To znamen�, ?e jestli na dan�m pinu bude nap?t� anebo nebude nap?t�, bude vstupn� hodnota anebo bude v�stupn�, m??eme ovliv?ovat p?es registr PORTC.
// Pin LED diody se v tom to registru nach�z� na �-t� pozici, tedy na pozici 6
// Pin tla?�tka se v tom to registru nach�z� na �-t� pozici, tedy na pozici 7


// Zadefinujeme makro LED, kter� bude reprezentovat n�? pin PC6 -> 6. bit na portu C
#define LED 6


int main(){
    PORTC.DIR = PORTC.OUT | (1 << LED); // Nastav�me v registru PORTC.DIR 6. bit na logickou jedni?ku -> nastav�me LED diodu jako OUTPUT (v�stup)
    PORTC.OUT = PORTC.OUT | (1 << LED); // Nastav�me ligickou jedni?ku na pin LED diody -> LED didoa je vypnut�

    PORTC.DIR = PORTC.DIR & ~(1 << 7); // Nastav�me v registru PORTC.DIR 7. bit na logickou nulu -> nastav�me tla?�tko jako INPUT (vstup)
    PORTC.PIN7CTRL = 0b00001000;       // Nastav�me na pin tla?�tka vnit?n� PULL-UP rezistor -> pin bude m�t po?�d nap?t� 3.3V (d�ky pull-up rezistoru) -> p?i zm�?knut� tla?�tka se nap?t� zm?n� na 0V

    while(1){
        // Pokud na sedm� pozici registru PORT.IN je hodnota 0 -> tla?�tko bylo zm�?knuto, pak prove? operace uvnit? v�razu if
        if(((PORTC.IN >> 7) & 1) == 0){
          PORTC.OUT = PORTC.OUT & ~(1 << LED); // Zapneme LED diodu
        }
        else{
          PORTC.OUT = PORTC.OUT | (1 << LED); // Vypneme LED diodu
        }
    }
    
    return 0;
}